import datetime
from typing import List, Dict

from lm_fritas_helper_work_report_generator.constants import DAYS_OF_THE_WEEK
from lm_fritas_helper_work_report_generator.utils import (is_date, is_vehicle_plate, is_night_shift)


class Tabela:
    key = None
    driver_pos = None
    value_pos = None

    def execute(self, data: Dict) -> List:
        return data[self.key]

    def _insert_date(self, data):
        COL_DATE = ['Data', 'Dia']
        new_data = []
        date = COL_DATE
        for row in data:
            agrouping = row[0]
            if date == COL_DATE:
                value = COL_DATE
                date = None
            elif is_date(agrouping):
                value = [agrouping, self._get_day(agrouping)]
                date = [agrouping, self._get_day(agrouping)]
            elif date:
                value = date
            else:
                value = ['', '']
            new_data.append(value + row)
        return new_data

    def _get_day(self, date):
        d, m, y = date.split('.')
        data = datetime.date(year=int(y), month=int(m), day=int(d))
        indice_da_semana = data.weekday()
        return DAYS_OF_THE_WEEK[indice_da_semana]

    def remove_duplicate_itens(self, data):
        new_data = []
        container = []
        for row in data:
            compare = row[3:]
            if compare not in container:
                new_data.append(row)
                container.append(compare)
        return new_data

    def filter_plate_itens(self, data):
        return list(filter(lambda x: is_vehicle_plate(x[2]), data))

    def filter_night_shift_itens(self, data):
        return list(filter(lambda x: is_night_shift(x[2]), data))

    def filter_driver(self, data, driver, index):
        return list(filter(lambda x: driver in x[index].replace(', ', ',').split(','), data))


class TabelaJornadaDiaria(Tabela):
    key = 'Diária'
    driver_pos = 6  # ajudante
    value_pos = 4


class TabelaHorarioRefeicao(Tabela):
    key = 'Horário de Refeição'
    driver_pos = 6  # ajudante
    value_pos = 4

    @classmethod
    def clean(cls, row):
        count = len(row[0].strip().split(' '))
        if len(row) == 4 and count > 1:
            row = row[0].strip().split(' ') + row[1:]
        return row

    # def execute(self, data: Dict) -> List:
    #     d = []
    #     for row in data[self.key]:
    #         if len(row) == 6 and not row[3]:
    #             row.remove('')
    #         if len(row) == 5:
    #             d.append(row)
    #     return d


class TabelaTEspera(Tabela):
    key = 'T. Espera'
    driver_pos = 6  # ajudante
    value_pos = 4


class TabelaHDirecao(Tabela):
    key = 'H. Direção'
    driver_pos = 3
    value_pos = 4


class TabelaInterjornada(Tabela):
    key = 'Interjornada'
    driver_pos = 6  # ajudante
    value_pos = 4


class TabelaPernoite(Tabela):
    key = 'Pernoite'
    driver_pos = 3  # ajudante
    value_pos = 6


class TabelaFimDaViagem(Tabela):
    key = 'Fim de Viagem'
    driver_pos = 3  # ajudante
    value_pos = 4


class TabelaMensagensCompleta(Tabela):
    key = 'Mensagens completa'
    driver_pos = 7  # ajudante

    def get_dates(self, data):
        dates = []
        for item in data:
            if item[:2] not in dates:
                dates.append(item[:2])
        return dates

