import csv
import locale
import re
from datetime import timedelta, datetime
from typing import List, Dict, Tuple

import openpyxl

from lm_fritas_helper_work_report_generator.constants import DATE_REGEX, DAYS_OF_THE_WEEK, TRAVEL_MESSAGES, MONTHS


def is_date(value):
    return re.search(DATE_REGEX, value)


def is_vehicle_plate(value):
    if value and not is_date(value) and not is_shift(value) and not is_day_of_the_week(value) and not is_field(value):
        return True
    return False


def is_shift(value):
    return value.lower() in ('diurno', 'noturno')


def is_day_of_the_week(value):
    return value.lower() in DAYS_OF_THE_WEEK


def is_field(value: str) -> bool:
    return 'agrupamento' in value.lower()


def is_trip(message, end):
    return message.lower() in TRAVEL_MESSAGES and len(end.split(' ')) == 1


def is_blank(row: List) -> bool:
    if not [i for i in row if i and i != '""']:
        return True
    return False


def is_new_table(row: List) -> bool:
    return row[0] and not any(row[1:]) and row[0] != '""'


def is_field_names(cache_: str, data_: Dict) -> bool:
    return cache_ in data_ and data_[cache_] == []


def is_night_shift(value):
    return value.lower() == 'noturno'


def convert_time_to_seconds(value: str) -> int:
    if not value:
        return 0
    if 'dia' in value:
        mo = re.search(r'[-]?(\d+) dia[s] ([0-9:]+)', value)
        if mo:
            hours_from_days = int(mo.group(1)) * 24
            hours, minutes, seconds = map(lambda x: int(x), mo.group(2).split(':'))
            value = f'{hours+hours_from_days}:{minutes}:{seconds}'

    is_negative = value.startswith('-')
    if is_negative:
        value = value[1:]

    hours, minutes, seconds = map(lambda x: int(x), value.split(':'))
    total_seconds = (hours * 3600) + (minutes * 60) + seconds
    return total_seconds if not is_negative else -total_seconds


def convert_to_time(value: str | None):
    if not value:
        value = '0:0:0'
    elif 'dia' in value:
        value = format_to_day_min_sec(convert_time_to_seconds(value))
    h, m, s = map(lambda x: int(x), value.split(':'))

    if h < 0:
        value_in_time = -timedelta(hours=h*-1, minutes=m, seconds=s)
    else:
        value_in_time = timedelta(hours=h, minutes=m, seconds=s)

    return value_in_time


def get_number_hours(duration):
    hour = '00'
    remaining_duration = 0
    number_hours = duration // 3600

    if number_hours > 0:
        hour = number_hours
        remaining_duration = duration - (number_hours*3600)

    return hour, remaining_duration


def get_number_minutes(duration):
    minute = '00'
    number_minutes = duration // 60
    if number_minutes > 0:
        minute = number_minutes
        remaining_duration = duration - (number_minutes * 60)
    else:
        remaining_duration = duration
    return minute, remaining_duration


def get_data_from_csv_file(filename: str, delimiter: str = ',') -> Dict:
    with open(filename, newline='') as csvfile:
        data = {}
        cache = None
        last_field = None

        spam_reader = csv.reader(csvfile, delimiter=delimiter, quotechar='"')
        for row in spam_reader:
            if is_blank(row):
                continue
            key = row[0]
            if is_new_table(row):
                last_field = table_name = key
                data[table_name] = []
            elif is_field_names(cache, data):
                fields = clean_fields(row)
                data[last_field].append(fields)
            else:
                data[last_field].append(clean(row, last_field))
            cache = key
    return data


def clean_fields(fields: List) -> List:
    ungroup = 'Agrupamento Início'
    if fields[0] == ungroup:
        fields = ungroup.split(' ') + fields[1:]
    elif fields[-2] == 'Jornada Diária Motorista':
        fields = fields[:-2] + ['Jornada Diária', 'Motorista']
    elif fields == 'Agrupamento,Início,Fim,,Duração,Motorista'.split(','):
        fields = 'Agrupamento,Início,Fim,Duração,Motorista'.split(',')
    elif fields[0] == 'Agrupamento Mensagens':
        fields = 'Agrupamento Mensagens'.split(' ') + fields[1:]
    return fields


def clean(row: List, key: str) -> List:
    if key == 'Horário de Refeição':
        mo = re.search(r'(.*?) ([0-9:]+)', row[0])
        if mo:
            row = [mo.group(1), mo.group(2)] + row[1:]

        if 'Código' in row[3]:
            mo = re.search(r'(.*?\d+:\d+:\d+) (.*? - Código \d+)', row[3])
            if mo:
                row = row[:-2] + [mo.group(1), mo.group(1)]

    elif key in ('Mensagens completa', 'Pausa Descanso'):
        if len(row[0].split(' ')) > 1:
            row = [row[0].split(' ')[0]] + [' '.join(row[0].split(' ')[1:])] + row[1:]
    return row


def format_number_with_two_digits(value: str | int) -> str:
    value = str(value)
    return value if len(value) >= 2 else '0' + value


def format_to_day_min_sec(duration: int, full_hours: int = 0) -> str:
    is_negative = duration < 0
    if is_negative:
        duration = duration * -1

    hour, remaining_duration = get_number_hours(duration)

    if remaining_duration:
        minute, remaining_duration = get_number_minutes(remaining_duration)
        second = str(remaining_duration)

    elif hour != '00' and not remaining_duration:
        minute = '0'
        second = '0'

    else:
        minute, remaining_duration = get_number_minutes(duration)
        second = str(remaining_duration)

    hour = full_hours + int(hour)
    hour = format_number_with_two_digits(hour)
    minute = format_number_with_two_digits(minute)
    second = format_number_with_two_digits(second)

    return f'{hour}:{minute}:{second}' if not is_negative else f'-{hour}:{minute}:{second}'


def formate_date_header(value):
    d, m, y = value.split('.')
    return f'{MONTHS[int(m)-1]} {y}'


def process_csv_file():
    import csv
    from lm_fritas_helper_work_report_generator import settings
    input_file = settings.CSV_FILE
    output_file = 'teste.csv'
    # Lista para armazenar as linhas processadas
    processed_rows = []
    with open(input_file, newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            processed_row = []
            for cell in row:
                # Verifica se a célula contém aspas e pelo menos uma vírgula
                if '"' in cell and ',' in cell:
                    cell = cell.replace(',', ';')
                processed_row.append(cell)
            processed_rows.append(processed_row)
    with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(processed_rows)


def calculate_night_time_in_seconds(start, end, increment=1):
    if end < start:
        raise Exception('Data inválida')
    count = 0

    if start.date() == end.date() and (start.hour, start.minute) == (end.hour, end.minute):
        if start.hour not in (22, 23, 0, 1, 2, 3, 4):
            return 0
        else:
            return end.second - start.second

    elif start.date() == end.date() and start.hour == end.hour:
        if start.hour not in (22, 23, 0, 1, 2, 3, 4):
            return 0
        else:
            return (end.minute * 60 + end.second) - (start.minute * 60 + start.second)

    new_datetime, start_diff_in_second = round_up_to_next_full_hour(start)
    end_datetime_around, end_diff_in_second = round_down_to_next_full_hour(end)

    while new_datetime < end_datetime_around:
        time_interval = timedelta(hours=increment)
        new_datetime = new_datetime + time_interval
        if new_datetime.time().hour in (23, 0, 1, 2, 3, 4, 5):
            count += 1
    return (count * 3600) + start_diff_in_second + end_diff_in_second


def calculate_night_time(start, end, increment=1):
    return format_to_day_min_sec(calculate_night_time_in_seconds(start, end, increment))


def round_up_to_next_full_hour(date_hour):
    diff_seconds = 0
    diff_minutes = 0

    # Verifica se os minutos e segundos da data são diferentes de zero
    if date_hour.minute != 0 or date_hour.second != 0:
        # Adiciona uma hora à data
        date_hour += timedelta(hours=1)
        if date_hour.hour in (22, 23, 0, 1, 2, 3, 4):
            diff_minutes = (59 - date_hour.minute) * 60
            diff_seconds = 60 - date_hour.second

        # Zera os minutos e segundos para torná-la uma hora cheia
        date_hour = date_hour.replace(minute=0, second=0)

    return date_hour, diff_minutes + diff_seconds


def round_down_to_next_full_hour(date_hour):
    diff = 0
    if date_hour.hour in (22, 23, 0, 1, 2, 3, 4):
        diff = (date_hour.minute * 60) + date_hour.second

    # Verifica se os minutos e segundos da data são diferentes de zero
    if date_hour.minute != 0 or date_hour.second != 0:
        # Zera os minutos e segundos para torná-la uma hora cheia
        date_hour = date_hour.replace(minute=0, second=0)

    return date_hour, diff


def generate_dates_with_weekdays(start_date_str, end_date_str):
    try:
        # Defina a localização para o Português Brasileiro
        locale.setlocale(locale.LC_TIME, 'pt_BR.utf8')

        start_date = datetime.strptime(start_date_str, '%d.%m.%Y')
        end_date = datetime.strptime(end_date_str, '%d.%m.%Y')

        if start_date > end_date:
            raise ValueError("A data inicial deve ser anterior à data final.")

        current_date = start_date
        while current_date <= end_date:
            day_of_week = current_date.strftime('%A')
            if day_of_week not in ('sábado', 'domingo'):
                day_of_week = f"{day_of_week}-feira"
            yield current_date.strftime('%d.%m.%Y'), day_of_week
            current_date += timedelta(days=1)
    except ValueError:
        raise ValueError("Formato de data inválido. Use DD.MM.AAAA")


def get_drivers():
    from lm_fritas_helper_work_report_generator.settings import CSV_FILE

    data_dict = get_data_from_csv_file(CSV_FILE)
    table = data_dict['Mensagens completa']
    drivers = [item[-1] for item in table[1:] if item[-1].strip()]

    drivers_ = []

    for driver in drivers:
        if ',' not in driver:
            drivers_.append(driver)
        else:
            for d in driver.split(','):
                drivers_.append(d.strip())

    drivers_ = list(set(drivers_))

    return drivers_

def get_worksheet_fields(wb, sheet_position: int = 2):
    return wb.sheetnames[sheet_position-1:]


def xlsx_to_list(wb, sheet_name: str) -> Tuple:
    ws = wb[sheet_name]
    rows = []
    for row in ws.iter_rows(values_only=True):
        rows.append(list(row))
    return sheet_name, rows


def xlsx_to_dict(wb, sheet_names: List) -> Dict:
    data = {}
    for sheet_name in sheet_names:
        key, rows = xlsx_to_list(wb, sheet_name)
        data[key] = rows
    return data


def get_data_from_xlsx_file(filename: str):
    wb = openpyxl.load_workbook(filename)
    fields = get_worksheet_fields(wb)
    data = xlsx_to_dict(wb, fields)
    wb.close()
    return data
