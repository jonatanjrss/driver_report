DATE_REGEX = r'\d{2}\.\d{2}\.\d{4}'

DAYS_OF_THE_WEEK = ('segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado', 'domingo')

TRAVEL_MESSAGES = ('inicio da jornada', 'inicio de viagem', 'reinicio de viagem',
                   'parada abastecimento', 'refeição', 'parada espera')

MONTHS = ('JAN', 'FEV', 'MAR', 'ABR', 'MAIO', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ')

MAX_COL = 11

START_ROW = 11
