import zipfile
from pathlib import Path

from bstrac_bot.__main__ import run

import lm_fritas_driver_work_report_generator
import lm_fritas_helper_work_report_generator
import cjk_driver_work_report_generator
import cjk_helper_work_report_generator
from web.exceptions import DriveNotFoundException


def correct_file_name(filename):
    extracted_file = filename.with_suffix('.xlsx')
    extracted_file = str(extracted_file)
    extracted_file = extracted_file.replace('[', '_').replace(']', '_')
    extracted_file_splited = extracted_file.split('.')
    extracted_file = '_'.join(extracted_file_splited[:-1]) + '.' + extracted_file_splited[-1]
    return extracted_file


def unzip_file(zip_file_path):
    destination_dir = zip_file_path.parent
    with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
        zip_ref.extractall(destination_dir)

        extracted_file = correct_file_name(zip_file_path)

        return Path(extracted_file).rename('./downloads/bstrac.xlsx')


def gerar_relatorio(modelo, objeto, tipo, motorista, data_inicial, data_final):
    zip_file_path = Path(run(modelo, objeto, motorista, data_inicial, data_final))
    file_path = str(unzip_file(zip_file_path))

    # CJK
    if 'cjk' in modelo.lower():
        if tipo == 'motorista':
            report_filename = cjk_driver_work_report_generator.report_generator(file_path, motorista)
        else:
            report_filename = cjk_helper_work_report_generator.report_generator(file_path, motorista)
    # LM FRITAS
    else:
        if tipo == 'motorista':
            report_filename = lm_fritas_driver_work_report_generator.report_generator(file_path, motorista)
        else:
            report_filename = lm_fritas_helper_work_report_generator.report_generator(file_path, motorista)

    try:
        with open(report_filename, 'rb') as excel_file:
            return excel_file.read()
    except FileNotFoundError:
        raise DriveNotFoundException
