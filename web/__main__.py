from datetime import timedelta

from web.app import app

app.run(debug=True)
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=30)
