import openpyxl
from openpyxl.styles import PatternFill, Font, Border, Side, Alignment
from openpyxl.drawing.image import Image
from openpyxl.drawing.spreadsheet_drawing import AbsoluteAnchor
from openpyxl.drawing.xdr import XDRPoint2D, XDRPositiveSize2D
from openpyxl.utils import get_column_letter
from openpyxl.utils.units import pixels_to_EMU

from cjk_helper_work_report_generator.api import (TabelaTEspera, TabelaJornadaDiaria,
                                                  TabelaInterjornada, TabelaHorarioRefeicao,
                                                  TabelaMensagensCompleta, TabelaPausaDescanso, TabelaInicioJornadaDiaria,
                                                  TabelaFimJornadaDiaria)
from cjk_helper_work_report_generator.night_time import calcule_nigth_time
from cjk_helper_work_report_generator.settings import OUTPUT_REPORT
from cjk_helper_work_report_generator.constants import MAX_COL, COL_JORNADA_DIARIA, COL_REFEICAO, COL_HORA_TRABALHADA, \
    COL_INTERJORNADA, FIRST_ROW
from cjk_helper_work_report_generator.utils import (convert_to_time, formate_date_header, format_to_day_min_sec,
                                                generate_dates_with_weekdays)

FORMAT_TIME = '[HH]:MM:SS'
BLACK = "000000"
GREY = 'ffededed'
ALIGN = Alignment(horizontal='center', vertical='center')
FILL = PatternFill(start_color=GREY, end_color=GREY, fill_type='solid')


def save(data_dict, driver, filename, logo_image):
    days = {}
    future_date = {}
    dates = get_dates(data_dict, driver)

    if not dates:
        print(f'Motorista "{driver}" não encontrado.')
        return f'Motorista "{driver}" não encontrado.'

    wb = openpyxl.load_workbook(filename)
    ws = wb[wb.sheetnames[0]]
    start_row = row = FIRST_ROW

    medium_border = Side(border_style="medium", color=BLACK)  # borda média preta

    date_header = formate_date_header(dates[0][0])

    for date, day in generate_dates_with_weekdays(dates[0][0], dates[-1][0]):
        data = f'{date} -\n{day.capitalize()}'
        diaria = get_value_v2(date, TabelaJornadaDiaria, data_dict, driver)
        inicio_jornada_diaria = get_value_v2(date, TabelaInicioJornadaDiaria, data_dict, driver)
        fim_jornada_diaria = get_value_v2(date, TabelaFimJornadaDiaria, data_dict, driver)
        refeicao = get_value_v2(date, TabelaHorarioRefeicao, data_dict, driver)
        t_espera = get_value_v2(date, TabelaTEspera, data_dict, driver)
        horas_trabalhadas = f'={COL_JORNADA_DIARIA}{row}-{COL_REFEICAO}{row}'
        hora_extra = f'=IF({COL_HORA_TRABALHADA}{row}<TIME(8,0,0), TIME(0,0,0) , {COL_HORA_TRABALHADA}{row}-TIME(8,0,0))'
        interjornada = get_value_v2(date, TabelaInterjornada, data_dict, driver)
        hora_indenizada = f'=IF(OR({COL_INTERJORNADA}{row}>=TIME(11,0,0), {COL_JORNADA_DIARIA}{row}=TIME(0,0,0)), TIME(0,0,0) , TIME(11,0,0)-{COL_INTERJORNADA}{row})'
        hora_noturna = calcule_nigth_time(date, TabelaMensagensCompleta, data_dict, driver, prepare_data, days).get(date) or 0

        hora_noturna = format_to_day_min_sec(hora_noturna)
        pausa_descanso = get_value_v2(date, TabelaPausaDescanso, data_dict, driver)

        ws.cell(row=row, column=1).value = data   # data
        ws.cell(row=row, column=2).value = convert_to_time(inicio_jornada_diaria)   # inicio_jornada_diaria
        ws.cell(row=row, column=3).value = convert_to_time(fim_jornada_diaria)   # fim_jornada_diaria
        ws.cell(row=row, column=4).value = convert_to_time(diaria)   # diária
        ws.cell(row=row, column=5).value = convert_to_time(refeicao)   # refeicao
        ws.cell(row=row, column=6).value = convert_to_time(t_espera)   # t_espera
        ws.cell(row=row, column=7).value = horas_trabalhadas if diaria != '0:0:0' else convert_to_time(None)  # horas trabalhadas
        ws.cell(row=row, column=8).value = hora_extra   # hora_extra
        ws.cell(row=row, column=9).value = convert_to_time(interjornada)   # interjornada
        ws.cell(row=row, column=10).value = hora_indenizada   # hora_indenizada
        ws.cell(row=row, column=11).value = convert_to_time(hora_noturna)   # hora_noturna
        ws.cell(row=row, column=12).value = convert_to_time(pausa_descanso)  # pausa_descanso

        format_cells(ws, row)

        row += 1

    # Data header
    ws.cell(row=FIRST_ROW-2, column=1).value = date_header

    # TOTAIS
    ws.cell(row=row, column=1).value = 'TOTAL DE \nHORAS'
    ws.cell(row=row, column=1).border = Border(right=medium_border, bottom=medium_border, left=medium_border, top=medium_border)
    ws.cell(row=row, column=1).alignment = ALIGN
    ws.cell(row=row, column=1).fill = FILL
    ws.cell(row=row, column=1).font = Font('Arial', size=10, bold=True)
    for col_index in range(2, MAX_COL):
        col_letter = get_column_letter(col_index)
        cell = ws.cell(row=row, column=col_index)
        cell.number_format = FORMAT_TIME
        if col_index in range(4, MAX_COL):
            cell.value = f'=subtotal(9, {col_letter}{start_row}:{col_letter}{row-1})'
        cell.border = Border(right=medium_border, bottom=medium_border, left=medium_border, top=medium_border)
        cell.alignment = ALIGN
        cell.fill = FILL

    ws.merge_cells(f'A{row}:C{row}')
    merged_cell = ws[f'A{row}']
    merged_cell.alignment = ALIGN

    ws.cell(row=5, column=1).value = f'MOTORISTA:  {driver}'
    ws.row_dimensions[row].height = 30

    add_image(ws, logo_image)

    output_file = OUTPUT_REPORT

    wb.save(output_file)

    wb.close()

    return output_file


def prepare_data(class_tabela, data_dict, driver, pos, filter_plate=True, filter_shift=False):
    tabela = class_tabela()
    data = tabela.execute(data=data_dict)
    data = tabela._insert_date(data)
    data = tabela.filter_driver(data, driver, pos)

    if filter_plate:
        data = tabela.filter_plate_itens(data)
    if filter_shift:
        data = tabela.filter_night_shift_itens(data)

    return data


def get_value(date, tabela_class, data_dict, driver, driver_index, value_pos, filter_plate=True, filter_shift=False):
    value = None
    for item in prepare_data(tabela_class, data_dict, driver, driver_index, filter_plate, filter_shift):
        if date == item[0]:
            value = item[value_pos]
            break
    return value


def get_value_v2(date, tabela_class, data_dict, driver, filter_plate=True, filter_shift=False):
    driver_pos = tabela_class.driver_pos + 1
    value_pos = tabela_class.value_pos + 1
    value = None
    for item in prepare_data(tabela_class, data_dict, driver, driver_pos, filter_plate, filter_shift):
        if date == item[0]:
            value = item[value_pos]
            break
    return value


def get_dates(data_dict, driver):
    tabela = TabelaMensagensCompleta()
    driver_index = tabela.driver_pos + 1
    data = tabela.execute(data=data_dict)
    data = tabela._insert_date(data)
    data = tabela.filter_plate_itens(data)
    data = tabela.filter_driver(data, driver, driver_index)
    return tabela.get_dates(data)


def format_cells(ws, row):
    for c in range(2, MAX_COL):
        ws.cell(row=row, column=c).number_format = FORMAT_TIME

    thin_border = Side(border_style="thin", color=BLACK)  # borda fina preta
    border = Border(right=thin_border, bottom=thin_border, left=thin_border, top=thin_border)
    for c in range(1, MAX_COL):
        cell = ws.cell(row=row, column=c)
        cell.border = border
        cell.alignment = ALIGN
        cell.fill = FILL

    ws.row_dimensions[row].height = 30


def add_image(ws, logo_image):
    p2e = pixels_to_EMU
    img = Image(logo_image)
    h, w = img.height, img.width

    position = XDRPoint2D(p2e(4), p2e(10))
    size = XDRPositiveSize2D(p2e(w), p2e(h))

    img.anchor = AbsoluteAnchor(pos=position, ext=size)
    ws.add_image(img)
