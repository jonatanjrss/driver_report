import os
import sys

from cjk_helper_work_report_generator.settings import REPORT_BASE, CSV_FILE, LOGO_IMAGE_PATH
from cjk_helper_work_report_generator.excel import save
from cjk_helper_work_report_generator.tabula_extractor import pdf_to_csv_with_tabula, pdf_to_csv_with_tabula_v2
from cjk_helper_work_report_generator.utils import get_data_from_csv_file, get_data_from_xlsx_file


def report_generator(filename, driver):
    if filename.lower().endswith('.pdf'):
        if not os.getenv("DEBUG"):
            pdf_to_csv_with_tabula(filename, CSV_FILE)
        data_dict = get_data_from_csv_file(CSV_FILE)
    elif filename.lower().endswith('.xlsx'):
        data_dict = get_data_from_xlsx_file(filename)
    else:
        print(f'O formato do arquivo "{filename}" não é suportado.')
        sys.exit()

    logo_image_path = LOGO_IMAGE_PATH
    output_file = save(data_dict, driver, REPORT_BASE, logo_image_path)
    return output_file
