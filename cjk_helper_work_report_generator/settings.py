import os


REPORT_BASE = os.path.join('cjk_helper_work_report_generator', 'assets', 'base.xlsx')
LOGO_IMAGE_PATH = os.path.join('cjk_helper_work_report_generator', 'assets', 'logo.png')
CSV_FILE = os.path.join('cjk_helper_work_report_generator', 'assets', 'tabula.csv')

OUTPUT_REPORT = os.path.join(r'/home/jonatan/upload/', 'cjk-relatorio.xlsx')
