import tabula


def pdf_to_csv_with_tabula(pdf_file: str, output_file: str,  pages: str = 'all') -> str:
    tabula.convert_into(pdf_file, output_file, output_format="csv", pages=pages)
    return output_file


def pdf_to_csv_with_tabula_v2(pdf_file: str, output_file: str,  pages: str = 'all') -> str:
    df = tabula.read_pdf(pdf_file, pages=pages)
    content = []
    for table in df:
        table.columns = ["" if col.startswith('Unnamed') else col for col in table.columns]
        text = table.to_csv(sep=';', index=False)
        content.append(text)
    with open(output_file, 'w') as fp:
        fp.write('\n'.join(content))
    return output_file
