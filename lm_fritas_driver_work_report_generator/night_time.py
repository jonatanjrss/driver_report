import re
from datetime import datetime, timedelta, time
from typing import Dict


class NightTime:
    MIDNIGHT = time(hour=0, minute=0, second=0)
    FIVE_HOURS = time(hour=5, minute=0, second=0)
    TWENTY_TWO_HOURS = time(hour=22, minute=0, second=0)
    LAST_HOUR_OF_DAY = time(hour=23, minute=59, second=59)

    def __init__(self, start_date: datetime, end_date: datetime):
        self.start_date = start_date
        self.end_date = end_date
        self.first_day = 0
        self.last_day = 0
        self.future_days = {}
        self.data_dict = {}

    def get_data_dict(self):
        return self.data_dict

    def update_data_dict(self, date, value):
        key = date.strftime('%d.%m.%Y')
        self.data_dict[key] = value

    def calcule(self) -> 'NightTime':
        if self.is_same_day(self.start_date, self.end_date):
            self.first_day = self.calcule_same_day(self.start_date, self.end_date)
            self.update_data_dict(self.start_date, self.first_day)

        elif self.is_consecutive_days(self.start_date, self.end_date):
            end_date = self.get_end_hour_from_date(self.start_date)
            self.first_day = self.calcule_same_day(self.start_date, end_date) + 1
            self.update_data_dict(self.start_date, self.first_day)

            start_date = self.get_start_hour_from_date(self.end_date)
            self.last_day = self.calcule_same_day(start_date, self.end_date)
            self.update_data_dict(start_date, self.last_day)

        else:
            end_date = self.get_end_hour_from_date(self.start_date)
            self.first_day = self.calcule_same_day(self.start_date, end_date) + 1
            self.update_data_dict(self.start_date, self.first_day)

            start_date = self.get_start_hour_from_date(self.end_date)
            self.last_day = self.calcule_same_day(start_date, self.end_date)
            self.update_data_dict(start_date, self.last_day)

            start_date = self.start_date.date()
            end_date = self.end_date.date()
            while True:
                start_date = start_date + timedelta(days=1)
                if not start_date < end_date:
                    break
                date_str = start_date.strftime('%d.%m.%Y')
                self.future_days[date_str] = 7 * 3600
                self.data_dict[date_str] = 7 * 3600

        return self

    @staticmethod
    def get_end_hour_from_date(date):
        return datetime.strptime(f'{date.strftime("%d.%m.%Y")} 23:59:59', '%d.%m.%Y %H:%M:%S')

    @staticmethod
    def get_start_hour_from_date(date):
        return datetime.strptime(f'{date.strftime("%d.%m.%Y")} 00:00:00', '%d.%m.%Y %H:%M:%S')

    def calcule_same_day(self, start_date, end_date):
        seconds = 0
        if self.is_between_5_and_22(start_date, end_date):
            seconds += 0
        elif self.is_between_0_and_5(start_date, end_date):
            seconds += self.time_to_seconds(end_date) - self.time_to_seconds(start_date)
        elif self.is_between_22_and_24(start_date, end_date):
            seconds += self.time_to_seconds(end_date) - self.time_to_seconds(start_date)
        elif self.is_before_and_after_5(start_date, end_date) and self.is_before_and_after_22(start_date, end_date):
            seconds += self.time_to_seconds(
                end_date.replace(hour=5, minute=0, second=0)) - self.time_to_seconds(start_date)
            seconds += self.time_to_seconds(end_date) - self.time_to_seconds(
                start_date.replace(hour=22, minute=0, second=0))
        elif self.is_before_and_after_22(start_date, end_date):
            seconds += self.time_to_seconds(end_date) - self.time_to_seconds(
                start_date.replace(hour=22, minute=0, second=0))
        elif self.is_before_and_after_5(start_date, end_date):
            seconds += self.time_to_seconds(
                end_date.replace(hour=5, minute=0, second=0)) - self.time_to_seconds(start_date)
        else:
            raise Exception('Unknown case')
        return seconds

    @classmethod
    def time_to_seconds(cls, date):
        return (3600 * date.hour) + (60 * date.minute) + date.second

    @classmethod
    def is_middle_night(cls, date):
        return date.strftime('%H:%M:%S') == '00:00:00'

    def is_before_and_after_22(self, start_date, end_date):
        return start_date.time() < self.TWENTY_TWO_HOURS < end_date.time()

    def is_before_and_after_5(self, start_date, end_date):
        return start_date.time() < self.FIVE_HOURS < end_date.time()

    def is_consecutive_days(self, start_date, end_date):
        return (start_date + timedelta(days=1)).day == end_date.day

    def is_same_day(self, start_date, end_date):
        return start_date.day == end_date.day

    def is_between_5_and_22(self, start_date, end_date):
        return (start_date.time() >= self.FIVE_HOURS) and (end_date.time() <= self.TWENTY_TWO_HOURS)

    def is_between_22_and_24(self, start_date, end_date):
        return (start_date.time() >= self.TWENTY_TWO_HOURS) and (end_date.time() <= self.LAST_HOUR_OF_DAY)

    def is_between_0_and_5(self, start_date, end_date):
        return (start_date.time() >= self.MIDNIGHT) and (end_date.time() <= self.FIVE_HOURS)


def calcule_nigth_time(date, TabelaMensagensCompleta, data_dict, driver, prepare_data_func, days) -> Dict:
    format_datetime = "%d.%m.%Y %H:%M:%S"
    data = prepare_data_func(TabelaMensagensCompleta, data_dict, driver, TabelaMensagensCompleta.driver_pos + 1)

    for item in data:
        if date == item[0]:
            mensagem = item[3].lower()
            if mensagem not in ('pernoite', 'refeição', 'parada espera', 'fim da jornada'):
                end_date_string = item[5]
                mo = re.search(r'(\d{2}\.\d{2}\.\d{4}) (\d{2}:\d{2}:\d{2})', end_date_string)
                start_date_string = f'{date} {item[4]}'
                if not mo:
                    end_date_string = f'{date} {item[5]}'
                else:
                    continue

                start_date = datetime.strptime(start_date_string, format_datetime)
                end_date = datetime.strptime(end_date_string, format_datetime)

                for key, value in NightTime(start_date, end_date).calcule().get_data_dict().items():
                    current_value = days.get(key, 0)
                    days[key] = current_value + value

    return days
