from playwright.sync_api import sync_playwright

from bstrac_bot.bstrac_bot import login, gerar_relatorio


def run(modelo, objeto, motorista, data_inicial, data_final):
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True, slow_mo=1000)
        context = browser.new_context()
        page = context.new_page()

        login(page, 'botbstrac', 'Botbstrac1*')

        output_filename = gerar_relatorio(
            browser,
            page,
            modelo=modelo,
            objeto=objeto,
            data_inicial=data_inicial,
            data_final=data_final,
        )

        return output_filename
