from typing import Tuple

import openpyxl


def xlsx_to_list(filename: str, sheet_position: int) -> Tuple:
    wb = openpyxl.load_workbook(filename)
    sheet_name = wb.sheetnames[sheet_position-1]
    ws = wb[sheet_name]
    rows = []
    for row in ws.iter_rows(values_only=True):
        rows.append(row)

    return sheet_name, rows


def xlsx_to_dict(filename):
    data = {}
    for sheet_position in range(2, 8+1):        
        key, rows = xlsx_to_list(filename, sheet_position)
        data[key] = rows
    return data


def get_worksheet_fields(filename: str, sheet_position: int = 2):
    wb = openpyxl.load_workbook(filename)
    return wb.sheetnames[sheet_position-1:]
