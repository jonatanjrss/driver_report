import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class FimViagemTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=8
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(75, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Motorista", "Inicio")
        _, values = self.data
        self.assertEqual(expected, values[0])

    def test_first_row(self):
        expected = ("01.09.2023", "FERNANDO NUNES FRANCA, WELINGTON RICARDO PRADO DE CAMARGO", "14:04:26")
        _, values = self.data
        self.assertEqual(expected, values[1])

    def test_last_row(self):
        expected = ("GFQ-1D55", "ANTONIO ONOFRE FIRMANI JUNIOR, DIEGO LIRA DE SOUZA", "15:03:56")
        _, values = self.data
        self.assertEqual(expected, values[-1])
