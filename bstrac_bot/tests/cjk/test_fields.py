import unittest
from pathlib import Path

from bstrac_bot.utils import get_worksheet_fields


class WorkSheetFieldsTest(unittest.TestCase):
    def test_fields(self):
        fields = get_worksheet_fields(filename=str(Path('bstrac_bot/tests/fixtures/cjk.xlsx')))
        expected = ['Diária', 'Horário de Refeição', 'T. Espera', 'H. Direção', 'Interjornada', 'Pausa Descanso',
                    'Mensagens completa', 'Fim da Jornada']
        self.assertEqual(expected, fields)
