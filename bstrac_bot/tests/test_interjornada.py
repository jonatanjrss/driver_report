import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class InterjornadaTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=6
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(75, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Início", "Fim", "Duração", "Motorista")
        _, values = self.data
        self.assertEqual(expected, values[0])


def test_first_row(self):
        expected = ("01.09.2023", "14:04:26", "05.09.2023 03:56:26", "140:01:10",
                    "FERNANDO NUNES FRANCA, WELINGTON RICARDO PRADO DE CAMARGO")
        _, values = self.data
        self.assertEqual(expected, values[1])


def test_last_row(self):
        expected = ("GFQ-1D55", "15:03:56", "05.10.2023 03:22:09", "12:18:13",
                    "ANTONIO ONOFRE FIRMANI JUNIOR, DIEGO LIRA DE SOUZA")
        _, values = self.data
        self.assertEqual(expected, values[-1])

