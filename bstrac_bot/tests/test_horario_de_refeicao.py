import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class HorarioRefeicaoTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=3
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(111, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Início", "Fim", "Duração", "Motorista")
        _, values = self.data
        self.assertEqual(expected, values[0])

    def test_first_row(self):
        expected = ("01.09.2023", "11:22:49", "19:46:17", "2:33:47",
                    "NILTON CESAR CAPONI, WELINGTON RICARDO PRADO DE CAMARGO")
        _, values = self.data
        self.assertEqual(expected, values[1])

    def test_last_row(self):
        expected = ("GFQ-1D55", "17:52:22", "17:57:11", "0:04:49",
                    "ANTONIO ONOFRE FIRMANI JUNIOR, JOSE THIAGO OLIVEIRA")
        _, values = self.data
        self.assertEqual(expected, values[-1])
