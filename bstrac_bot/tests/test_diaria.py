import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class DiariaTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=2
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(82, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Início da Jornada", "Fim da Jornada", "Jornada Diária", "Motorista")
        _, values = self.data
        self.assertEqual(expected, values[0])

    def test_first_row(self):
        expected = ("01.09.2023", "02:52:37", "21:06:32", "1 dias 3:07:28",
                    "NILTON CESAR CAPONI, WELINGTON RICARDO PRADO DE CAMARGO")
        _, values = self.data
        self.assertEqual(expected, values[1])

    def test_last_row(self):
        expected = ("GFQ-1D55", "03:22:09", "18:43:31", "14:44:45",
                    "ANTONIO ONOFRE FIRMANI JUNIOR, JOSE THIAGO OLIVEIRA")
        _, values = self.data
        self.assertEqual(expected, values[-1])
