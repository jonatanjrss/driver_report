import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class TEsperaTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=4
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(82, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Início", "Fim", "Duração", "Motorista")
        _, values = self.data
        self.assertEqual(expected, values[0])

    def test_first_row(self):
        expected = ("01.09.2023", "03:59:32", "20:55:46", "9:24:58",
                    "NILTON CESAR CAPONI, WELINGTON RICARDO PRADO DE CAMARGO")
        _, values = self.data
        self.assertEqual(expected, values[1])

    def test_last_row(self):
        expected = ("GFQ-1D55", "06:40:06", "18:06:03", "7:07:02", "ANTONIO ONOFRE FIRMANI JUNIOR")
        _, values = self.data
        self.assertEqual(expected, values[-1])
