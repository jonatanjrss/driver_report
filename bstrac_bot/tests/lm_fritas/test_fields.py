import unittest
from pathlib import Path

from bstrac_bot.utils import get_worksheet_fields


class WorkSheetFieldsTest(unittest.TestCase):
    def test_fields(self):
        fields = get_worksheet_fields(filename=str(Path('bstrac_bot/tests/fixtures/lm_fritas.xlsx')))
        expected = ['Diária', 'Horário de Refeição', 'T. Espera', 'H. Direção', 'Interjornada', 'Mensagens completa',
                    'Pernoite', 'Fim de Viagem']
        self.assertEqual(expected, fields)
