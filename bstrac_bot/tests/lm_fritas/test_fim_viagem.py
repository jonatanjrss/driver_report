import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class FimViagemTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/lm_fritas.xlsx')),
            sheet_position=9
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(163, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Motorista", "Inicio")
        _, values = self.data
        self.assertEqual(expected, values[0])
