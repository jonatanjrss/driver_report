import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


filename = str(Path('bstrac_bot/tests/fixtures/lm_fritas.xlsx'))


class TEsperaTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/lm_fritas.xlsx')),
            sheet_position=4
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(302, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Início", "Fim", "Duração", "Motorista")
        _, values = self.data
        self.assertEqual(expected, values[0])
