import unittest
from pathlib import Path

from bstrac_bot.utils import xlsx_to_list


class HoraDirecaoTest(unittest.TestCase):
    def setUp(self):
        self.data = xlsx_to_list(
            filename=str(Path('bstrac_bot/tests/fixtures/teste.xlsx')),
            sheet_position=5
        )

    def test_count(self):
        _, values = self.data
        self.assertEqual(179, len(values))

    def test_fields(self):
        expected = ("Agrupamento", "Motorista", "Duração")
        _, values = self.data
        self.assertEqual(expected, values[0])

    def test_first_row(self):
        expected = ("01.09.2023", "NILTON CESAR CAPONI, WELINGTON RICARDO PRADO DE CAMARGO", "41:08:45")
        _, values = self.data
        self.assertEqual(expected, values[1])

    def test_last_row(self):
        expected = ("GFQ-1D55", "ANTONIO ONOFRE FIRMANI JUNIOR, JOSE THIAGO OLIVEIRA", "9:02:02")
        _, values = self.data
        self.assertEqual(expected, values[-1])
