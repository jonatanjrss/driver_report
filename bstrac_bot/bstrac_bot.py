from pathlib import Path


#DOWNLOAD_DIR = "/home/jonatan/workspace/drivers/"
DOWNLOAD_DIR = "./downloads/"
TYPE_DELAY = .3
RELATORY_DELAY = 10000


def login(page, username, password):
    url = 'http://rastreamento.bstrac.com.br/'
    print(f'Acessando {url} ...')
    page.goto(url)
    page.fill('#user', username)
    page.fill('#passw', password)    
    page.locator('#submit').click()
    print('Fazendo login')


def gerar_relatorio(browser, page, modelo, objeto, data_inicial, data_final, output_filename=None):
    print('Gerando relatório')
    page.wait_for_timeout(RELATORY_DELAY)  # TODO esperar elemento ficar visivel

    # clica na aba relatório
    page.locator('#hb_mi_reports_ctl').click()

    # seleciona o modelo
    page.locator('.textboxarrow').first.click()
    page.keyboard.type(modelo, delay=TYPE_DELAY)
    page.keyboard.press('Enter')

    # Seleciona o objeto
    page.locator('.textboxarrow').nth(1).click()
    page.keyboard.type(objeto, delay=TYPE_DELAY)
    page.keyboard.press('Enter')

    # Define o intervalo
    page.fill('#time_from_report_templates_filter_time', data_inicial)
    page.fill('#time_to_report_templates_filter_time', data_final)

    # executa o relatório
    page.locator('#report_templates_filter_params_execute').click()
    page.locator('.export-control').nth(1).click()

    # ---------XLSX----------
    page.get_by_text("arquivo").click()
    page.locator('//span[@data-translate-phrase="excel"]').click()
    # ---------XLSX----------

    # ---------CSV----------
    # # Define o formato do arquivo de saída
    # page.get_by_text("arquivo").click()
    # page.locator('//input[@data-element="csvTypeCheckboxEl"]').click()

    # # Define o separador
    # page.locator('//select[@data-element="delimiterSelectEl"]').click()
    # page.keyboard.press('ArrowUp')
    # page.keyboard.press('Enter')
    # ---------CSV----------

    # Baixa o relatório
    with page.expect_download() as download_info:
        page.locator('#wizard_dlg_btn_ok').click()
    download = download_info.value
    dst = Path(DOWNLOAD_DIR)
    filename = output_filename or download.suggested_filename
    download.save_as(dst / filename)

    # Fecha o browser
    browser.close()

    return dst / filename
