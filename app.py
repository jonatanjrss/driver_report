from flask import Flask, render_template, request, Response

from web import services
from web.exceptions import DriveNotFoundException

app = Flask(__name__)

# Opções para o campo de seleção do modelo e objeto
model_options = ['Relatório de Grupo - Fritas', 'Jornada Grupo - CJK']
object_options = ['[LM FRITAS]', '[Jornada]']


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        modelo = request.form.get('modelo')
        objeto = request.form.get('objeto')
        tipo = request.form.get('tipo')
        motorista = request.form.get('motorista')
        data_inicial = request.form.get('data_inicial') + ' 00:00'
        data_final = request.form.get('data_final') + ' 23:59'

        try:
            excel_data = services.gerar_relatorio(modelo, objeto, tipo, motorista, data_inicial, data_final)
        except DriveNotFoundException:
            return 'Motorista/ajudante não encontrado'
        else:
            response = Response(excel_data,
                                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            output_filename = motorista.replace(' ', '_')
            response.headers["Content-Disposition"] = f"attachment; filename={output_filename}.xlsx"
            return response

    return 'Olá mundo!'
