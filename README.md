## Requisitos:
- python 3.11


## Instalação - desenvolvimento ou demonstração
```shell
# clone o projeto
git clone https://gitlab.com/jonatanjrss/driver_report
cd driver_report

# crie um ambiente virtual
python -m venv .venv

# ative o ambiente virtual
source .venv/bin/activate

# atualize o pip
python -m pip install -U pip

# instale as dependências
pip install -r requirements.txt
 ```

## Execução
```shell
python -m web 
```

### TODO
- exibir mensagem quando download for concluído
- remover arquivos baixados


## Para rodar localmente, mas deixar acessível na internet com o objetivo de fazer uma demonstração recomendo usar o ngrok

Link de download: https://ngrok.com/download

Depois use o comando abaixo:
```shell
ngrok http 5000
```
